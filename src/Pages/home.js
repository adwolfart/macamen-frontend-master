import React, { useEffect, useState } from 'react';
import { getCalendario } from "../Services/home";
import {InfoCard} from "../components/infocard"
import Header from '../components/header';
import { Footer } from "../components/footer";
import Carousel  from "../components/carousel"
import DateList from '../components/datelist';
import InfoCardHolder from '../components/infocardlist';
export function Home() {
    const [dataCalendario, setDataCalendario] = useState(0);

    /**
     * Estado 
     */
    useEffect(() => {        
        //axios.post('http://localhost:3000/login/temp',
        getCalendario().then(function (response) {
             console.log(response.data);
            // console.log(response.data.data);
            setDataCalendario(response.data.data);
        }).catch(function (error) {
            console.log(error);
        });


    }, [setDataCalendario]);

    return (
        <>
            {/**
             * <div>mi Token: {dataCalendario}</div>

             
            
            <InfoCard 
                
            />*/}
            <Header></Header>
            <Carousel></Carousel>
            <DateList ></DateList>
            <InfoCardHolder data={dataCalendario}></InfoCardHolder>
            <Footer></Footer>
        </>

    )
}