import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import { useState } from 'react';
import home from "./Pages/home";
import LoginForm from './components/loginform';
import axios from 'axios';
import Navbar from './components/Navbar';



function App() {
  const [state, setstate] = useState({logged:false})  
  const handleLogin=(usr,pass)=>{
    {/*Falta sacar el log in*/}
  }


  return (
   

   <Router>
     <Navbar />
      <div>
        

        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Switch>
          
          
          <Route exact path="/" >
            {state.logged==true? <Redirect to="/login"/>:<home />}
            
          </Route>
          <Route path="/privada">
            <div>privada 1</div>
          </Route>
          <Route path="/privada2">
          <div>privada 2</div>
          </Route>
          <Route path="/login">

            <LoginForm Login={handleLogin}></LoginForm>
          </Route>
          <Route path="*">
            <p>404</p>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
