import React from 'react';
import Date from './date';

export function DateList(props) {
    
    return(
        <div className="date-list-containter"
            >
            {props.data}
        </div>
    )
}

export default DateList;