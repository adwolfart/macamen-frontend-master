import React from 'react';
import bed from '../Assets/bed.png'
export function Header(props) {

    return (
        
            <div className="header">
                <div className="header-logo">
                    <img src={bed} alt="" />

                    <div className="header-menu">
                        <ul>
                            <li>
                                opcion1
                            </li>
                            <li>
                                opcion2
                            </li>
                            <li>
                                opcion3
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        
    )

}
export default Header;