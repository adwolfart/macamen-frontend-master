import React, {useState} from 'react';

export function LoginForm(props){
    const [state, setstate] = useState({email:"",pasword:""})
    const handlechange=(e)=>{
        setstate({...state,
            [e.target.name]:e.target.value})
                    
    }
    const login=(e)=>{
        props.handleLogin(state)
    }
    return(
        <form onSubmit={login}>
            <label >Email</label>
            <input type="text" name="email" 
            placeholder="Email"
            value={state.email}
            onChange={handlechange}/>
            <label >Password:</label>
            <input type="password" name="pass"
            placeholder=""
            onChange={handlechange}/>
            <button className="card-button" type="submit"   >Entrar</button>
        </form>
        

        
    )
}

export default LoginForm;

