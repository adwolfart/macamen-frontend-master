import React from 'react';
import { Link } from 'react-router-dom';

export function InfoCard(props) {
    return(
        <div className="info-card">
            <div className="card-img-container">
                <img src={props.image_url} alt="" className="card-image"/>
            </div>
            <p className="card-title">{props.title}</p>
            <p className="card-info">{props.info}.</p>
            <Link to={props.link}></Link>
        </div>
    )
}

export default InfoCard;